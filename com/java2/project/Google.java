package com.java2.project;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.Color;
import java.awt.Font;

public class Google {

	JFrame Google;
	private JTextField textField;
	private JTextField txtSearchGoogleOr;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Google window = new Google();
					window.Google.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Google() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Google = new JFrame();
		Google.setBounds(100, 100, 965, 549);
		Google.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Google.getContentPane().setLayout(null);
		
		txtSearchGoogleOr = new JTextField();
		txtSearchGoogleOr.setFont(new Font("Arial", Font.PLAIN, 12));
		txtSearchGoogleOr.setText("Search Google or type URL");
		txtSearchGoogleOr.setForeground(Color.BLACK);
		txtSearchGoogleOr.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(txtSearchGoogleOr.getText().equals("Search Google or type URL")) {
					txtSearchGoogleOr.setText("");
				}
				else {
					txtSearchGoogleOr.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(txtSearchGoogleOr.getText().equals(""))
					txtSearchGoogleOr.setText("Search Google or type URL");
			}
		});
		txtSearchGoogleOr.setBorder(null);
		txtSearchGoogleOr.setBounds(303, 311, 349, 20);
		Google.getContentPane().add(txtSearchGoogleOr);
		txtSearchGoogleOr.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\USER\\Downloads\\download (3).png"));
		lblNewLabel.setBounds(0, 0, 950, 512);
		Google.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBorder(null);
		textField.setBounds(107, 36, 728, 20);
		Google.getContentPane().add(textField);
		textField.setColumns(10);
	}

	public void setVisible(boolean b) {
		// TODO Auto-generated method stub
		
	}
}
