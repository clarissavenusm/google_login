package com.java2.project;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.JTextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;

public class Register {

	JFrame Register;
	private JPasswordField password;
	private JPasswordField confirmpassword;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JLabel lblSignupMesssage2 = new JLabel("");
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_4;
	private JTextField Username;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register window = new Register();
					window.Register.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Register() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Register = new JFrame();
		Register.setBounds(100, 100, 691, 543);
		Register.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Register.getContentPane().setLayout(null);
		
		password = new JPasswordField();
		password.setEchoChar((char)0);
		password.setFont(new Font("Arial", Font.PLAIN, 12));
		password.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(password.getText().equals("Password")) {
					password.setEchoChar('●');
					password.setText("");
					}
					else {
						password.selectAll();
					}
				}
				@Override
				public void focusLost(FocusEvent e) {
					if(password.getText().equals(""))
						password.setText("Password");
						password.setEchoChar((char)0);
			}
		});
		
		confirmpassword = new JPasswordField();
		confirmpassword.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(confirmpassword.getText().equals("Confirm Password")) {
					confirmpassword.setEchoChar('●');
					confirmpassword.setText("");
					}
					else {
						confirmpassword.selectAll();
					}
				}
				@Override
				public void focusLost(FocusEvent e) {
					if(confirmpassword.getText().equals(""))
						confirmpassword.setText("Confirm Password");
						confirmpassword.setEchoChar((char)0);
			}
		});
		
		Username = new JTextField();
		Username.setBorder(null);
		Username.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(Username.getText().equals("Username")) {
					Username.setText("");
				}
				else {
					Username.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(Username.getText().equals(""))
					Username.setText("Username");
			}
		});
		
		JLabel lblgmailcom = new JLabel("@gmail.com");
		lblgmailcom.setBorder(null);
		lblgmailcom.setFont(new Font("Arial", Font.BOLD, 12));
		lblgmailcom.setBounds(330, 203, 91, 35);
		Register.getContentPane().add(lblgmailcom);
		Username.setText("Username");
		Username.setFont(new Font("Arial", Font.BOLD, 12));
		Username.setBounds(79, 203, 245, 34);
		Register.getContentPane().add(Username);
		Username.setColumns(10);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNewLabel_1.setBounds(67, 194, 355, 44);
		Register.getContentPane().add(lblNewLabel_1);
		confirmpassword.setEchoChar((char)0);
		confirmpassword.setFont(new Font("Arial", Font.PLAIN, 12));
		confirmpassword.setText("Confirm Password");
		confirmpassword.setBounds(216, 308, 150, 35);
		Register.getContentPane().add(confirmpassword);
		password.setText("Password");
		password.setBounds(56, 308, 150, 35);
		Register.getContentPane().add(password);
		
		txtFirstName = new JTextField();
		txtFirstName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(txtFirstName.getText().equals("First name")) {
					txtFirstName.setText("");
				}
				else {
					txtFirstName.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(txtFirstName.getText().equals(""))
					txtFirstName.setText("First name");
			}
		});
		txtFirstName.setText("First name");
		txtFirstName.setFont(new Font("Arial", Font.PLAIN, 12));
		txtFirstName.setBounds(46, 150, 177, 35);
		Register.getContentPane().add(txtFirstName);
		txtFirstName.setColumns(10);
		
		JButton btnNewButton = new JButton("SIGN UP");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(confirmpassword.getText().equals(password.getText())) {
					//if correct user input
				Google sf = new Google();
				sf.Google.setVisible(true);
				Register.dispose();
				}else {
					lblSignupMesssage2.setText("Password didn't match");
				}
			}
		});
		btnNewButton.setFont(new Font("Arial", Font.BOLD, 12));
		btnNewButton.setBackground(new Color(0, 102, 255));
		btnNewButton.setBounds(317, 403, 91, 44);
		Register.getContentPane().add(btnNewButton);
		
		JButton btnSignInInstead = new JButton("Sign in instead");
		btnSignInInstead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SignUp sf = new SignUp();
				sf.LOGIN.setVisible(true);
				Register.dispose();
			}
		});
		btnSignInInstead.setBorder(null);
		btnSignInInstead.setForeground(SystemColor.textHighlight);
		btnSignInInstead.setFont(new Font("Arial", Font.BOLD, 12));
		btnSignInInstead.setBackground(Color.WHITE);
		btnSignInInstead.setBounds(55, 427, 125, 44);
		Register.getContentPane().add(btnSignInInstead);
		
		txtLastName = new JTextField();
		txtLastName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				if(txtLastName.getText().equals("Last name")) {
					txtLastName.setText("");
				}
				else {
					txtLastName.selectAll();
				}
			}
			@Override
			public void focusLost(FocusEvent e) {
				if(txtLastName.getText().equals(""))
					txtLastName.setText("Last name");
			}
		});
		txtLastName.setText("Last name");
		txtLastName.setColumns(10);
		txtLastName.setBounds(231, 150, 177, 35);
		Register.getContentPane().add(txtLastName);
		lblSignupMesssage2.setBorder(null);
		lblSignupMesssage2.setFont(new Font("Arial", Font.PLAIN, 12));
		lblSignupMesssage2.setBounds(121, 286, 208, 20);
		Register.getContentPane().add(lblSignupMesssage2);
		
		lblNewLabel_2 = new JLabel("You can use letters, numbers & periods");
		lblNewLabel_2.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_2.setBorder(null);
		lblNewLabel_2.setBounds(71, 244, 231, 20);
		Register.getContentPane().add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("Use my current email address instead");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 12));
		lblNewLabel_3.setForeground(SystemColor.textHighlight);
		lblNewLabel_3.setBorder(null);
		lblNewLabel_3.setBounds(73, 268, 239, 20);
		Register.getContentPane().add(lblNewLabel_3);
		
		lblNewLabel_4 = new JLabel("Use 8 or more characters with a mix of letters, numbers & symbols");
		lblNewLabel_4.setFont(new Font("Arial", Font.PLAIN, 12));
		lblNewLabel_4.setBounds(56, 354, 368, 24);
		Register.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\USER\\Downloads\\gmail-sign-up-form.png"));
		lblNewLabel.setBounds(0, 0,675, 504);
		Register.getContentPane().add(lblNewLabel);
	}
}
